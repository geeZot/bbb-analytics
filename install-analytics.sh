#! /bin/bash
#$1=site Directory
#$2=Environnement master/dev
git clone git@framagit.org:geeZot/bbb-analytics.git /var/www/$1

cd /var/www/$1
/usr/bin/composer self-update
/usr/bin/composer install

bin/console doctrine:migration:sync
bin/console doctrine:migrations:migrate --no-interaction

bin/console asset:install
chmod -R 755 app/cache
chmod -R 755 web/uploads
chown -R www-data:www-data app/cache
chown -R www-data:www-data app/logs
