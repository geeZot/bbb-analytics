<?php

namespace App\Entity;

use App\Repository\MeetingRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: MeetingRepository::class)]
class Meeting
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $breakout = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $start = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $end = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $reason = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $filename = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $externalid = null;

    #[ORM\Column(type: 'dateinterval')]
    private $duree;
    
    /**
     * @var \Doctrine\Common\Collections\Collection<\App\Entity\Joiner>
     */
    #[ORM\OneToMany(targetEntity: 'Joiner', mappedBy: 'meeting')]
    private \Doctrine\Common\Collections\Collection $joiners;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $importedDate = null;
    
    public function __construct() {
        $this->joiners = new ArrayCollection();
        $this->importedDate = new \DateTime();
    }

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBreakout(): ?bool
    {
        return $this->breakout;
    }

    public function setBreakout(bool $breakout): self
    {
        $this->breakout = $breakout;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getExternalid(): ?string
    {
        return $this->externalid;
    }

    public function setExternalid(string $externalid): self
    {
        $this->externalid = $externalid;

        return $this;
    }

    public function getDuree(): ?\DateInterval
    {
        return $this->duree;
    }

    public function setDuree(\DateInterval $duree): self
    {
        $this->duree = $duree;

        return $this;
    }
    
//    /**
//     * @param Joiner $joiner
//     */
//    public function addJoiner(Joiner $joiner) {
//            $this->joiners->add($joiner);
//    }
    
    
    /**
     * @return ArrayCollection $joiners
     */
    public function getJoiners() {
        return $this->joiners;
    }

    public function getImportedDate(): ?\DateTimeInterface
    {
        return $this->importedDate;
    }

    public function setImportedDate(\DateTimeInterface $importedDate): self
    {
        $this->importedDate = $importedDate;

        return $this;
    }
}
