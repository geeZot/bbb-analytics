<?php

namespace App\Entity;

use App\Repository\JoinerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JoinerRepository::class)]
class Joiner
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $externalid = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $role = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $jointime = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $lefttime = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;
    
    
    /**
     * @var \App\Entity\Meeting
     */
    #[ORM\JoinColumn(name: 'meeting_id', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: 'Meeting', inversedBy: 'joiners')]
    private ?\App\Entity\Meeting $meeting = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternalid(): ?string
    {
        return $this->externalid;
    }

    public function setExternalid(?string $externalid): self
    {
        $this->externalid = $externalid;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getJointime(): ?\DateTimeInterface
    {
        return $this->jointime;
    }

    public function setJointime(\DateTimeInterface $jointime): self
    {
        $this->jointime = $jointime;

        return $this;
    }

    public function getLefttime(): ?\DateTimeInterface
    {
        return $this->lefttime;
    }

    public function setLefttime(\DateTimeInterface $lefttime): self
    {
        $this->lefttime = $lefttime;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    function getMeeting(): \App\Entity\Meeting {
        return $this->meeting;
    }

    function setMeeting(\App\Entity\Meeting $meeting) {
        $this->meeting = $meeting;
        return $this;
    }


    
}
