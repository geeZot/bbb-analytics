<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\GeneratorInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Knp\Snappy\Pdf;
use App\Entity\Meeting;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Doctrine\Persistence\ManagerRegistry;
use Qipsius\TCPDFBundle\Controller\TCPDFController;

use App\Entity\Joiner;

class DefaultController extends AbstractController
{
    protected $managerRegistry;
    protected $tcpdf;


    public function __construct(ManagerRegistry $managerRegistry, TCPDFController $tcpdf) {
        $this->managerRegistry = $managerRegistry;
        $this->tcpdf = $tcpdf;
    }
    /**
     * @Route("/default", name="default")
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
     
        $start = $request->query->get('dateStart');
        $end = $request->query->get('dateEnd');
        $filterField = $request->query->get('filterField');
        $filterValue = $request->query->get('filterValue');
        $em = $this->managerRegistry->getManager();
        
        if((!is_null($start) && !is_null($end)) && ($start <= $end)) {
            $meetings = $em->getRepository('App\Entity\Meeting')->filterByDate(
                    new \DateTime($start.' 00:00:00'),
                    new \DateTime($end.' 23:59:59'),
//                    is_null($request->query->get('filterValue'))?FALSE:TRUE,
                    FALSE,
                    [$filterField => $filterValue]
                    );
        } else {
            $meetings = $em->getRepository('App\Entity\Meeting')->findBy([],array(
                'start' => 'DESC'
            ));
        }
        
        $meetingsPage = $paginator->paginate(
            $meetings, 
            $request->query->getInt('page',1) /*page number*/,
            $request->query->getInt('limit',10) /*limit per page*/
        );
        
        return $this->render('default/index.html.twig', [
            'meetings' => $meetingsPage,
            'dateStart' => $request->query->get('dateStart'),
            'dateEnd' => $request->query->get('dateEnd')
        ]);
    }
    
    public function print (Request $request, Pdf $pdf) {
        
        $em = $this->managerRegistry->getManager();
        
        $start = $request->request->get('dateStart');
        $end = $request->request->get('dateEnd');
        
        $filterField = $request->request->get('filterField');
        $filterValue = $request->request->get('filterValue');

//        dd($request->request->get('filterField'));
        
        if((!empty($start) && !empty($end)) AND ($start <= $end)) {
            $meetings = $em->getRepository('App\Entity\Meeting')->filterByDate(
                    new \DateTime($start.' 00:00:00'),
                    new \DateTime($end.' 23:59:59'),
                    FALSE,
                    [$filterField => $filterValue]
                    );
        } else {
            $meetings = $em->getRepository('App\Entity\Meeting')->findAll();
        }
            $html = $this->renderView(
                'default/print.html.twig',
                array(
                    'meetings'  => $meetings
                )
            );

            return new PdfResponse(
                $pdf->getOutputFromHtml($html),
                    'file.pdf'
            ); 
    }
    
     /**
     * @Route("/export/{filename}", name="export_users")
     */
    public function userExport(string $filename)
    {
        $em = $this->managerRegistry->getManager();
        $meeting = $em->getRepository('App\Entity\Meeting')->findOneBy(['filename'=>$filename]);
        $joiners = $em->getRepository('App\Entity\Joiner')->findBy(['meeting'=>$meeting]);
        
        $encoders = [new CsvEncoder()];
        $normalizers = [new DateTimeNormalizer(['datetime_format'=>'H:i:s','datetime_timezone'=>'Europe/Paris']), new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        
        $csvContent = implode(';',[
                    $meeting->getName(),
                    $meeting->getStart()->format('d-m-Y')
                ]
                
                ).chr(10);
        
        $csvContent .= $serializer->serialize($joiners, 'csv', [
            'csv_delimiter'=>';',
            'attributes' => ['name','role','jointime','lefttime'],
//            'circular_reference_handler' => function ($object) {
//                    return $object->getId();
//            }
            ]);
        
        $response = new Response($csvContent);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename=participants.csv');
        
        return $response;
    }
    
    /**
     * @Route("/presences", name="presences")
     */
    
    function getPresences(){
        
        $em = $this->managerRegistry->getManager();
        $joiners = $em->getRepository('App\Entity\Joiner')->getListJoiners();
        return $this->render('default/presences.html.twig', 
                [
                    'joiners'=>$joiners
                ]);
    }
    
    
    /**
     * @Route("/printpresences", name="printpresences", options={"expose"=true}, methods={"POST"})
     * @param Request $request
     */
    
    public function printPresenceJoiners(Request $request) : Response {
        
        if (!$this->getUser()) {
             return $this->redirectToRoute('default');
        }
        
        $joiners = $request->request->get('joiners');
        
        $dateStart = date_create($request->request->get('start'))->setTime(00, 00, 00);
        $dateEnd = date_create($request->request->get('end'))->setTime(23, 59, 59);
        
        $em = $this->managerRegistry->getManager();

        $filename = $this->genPDFJoiners($joiners, $dateStart, $dateEnd);
        
        $responce = new Response();
        $responce->setContent($filename);
        return $responce;
    }
    
    private function genPDFJoiners($joiners, $dateStart, $dateEnd) {
        
        $em = $this->managerRegistry->getManager();
        
        $pdf = $this->tcpdf->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//        $pdf->SetAuthor($this->getParameter('plateforme_name'));
        $pdf->SetTitle(('Feuille de présences par participant '));
        $pdf->SetSubject('Présences individuelles');
        $pdf->setFontSubsetting(true);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        
        $upload =  $this->getParameter('kernel.project_dir').'/public/uploads';

        $pdf->SetHeaderData('',
                10,
                'bbb-analytics'
                );
        
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetFont('helvetica', '', 11, '', true);
        $filename = 'Feuille_presences_eleves-'. uniqid().'.pdf';
            
        $presencesJoiners = $em->getRepository('App\Entity\Joiner')->getPresencesByJoiner($joiners, $dateStart, $dateEnd);
        
        $activeJoiner = "";
        
        foreach($presencesJoiners as $presence) {

                if(empty($activeJoiner) || $activeJoiner != $presence->getExternalId() ) {
                    if(!empty($activeJoiner)) {
                        $pdf->writeHTMLCell($w = 150,  0, '', '', 'Total heures ', 1, 0, 0, true, 'R', true);
                        $pdf->writeHTMLCell($w = 40, 0, 160, '', $totalHeuresOri->diff($totalHeures)->h + ($totalHeuresOri->diff($totalHeures)->days * 24).'H '. $totalHeuresOri->diff($totalHeures)->i, 1, 1, 0, true, 'C', true);
                    }
                    $activeJoiner = $presence->getExternalId();
                    $totalHeuresOri = new \DateTime('00:00');
                    $totalHeures = clone $totalHeuresOri;
                    $pdf->AddPage();
                }
                
                    if(($pdf->getY() <= 11)||($pdf->getY() >= 267)){
                        $pdf->writeHTMLCell(0, 0, '', '', $presence->getName(), 1, 1,0, true, 'C', true);
                        $pdf->writeHTMLCell($w = 30, 0, '', '', 'Date', 1, 0, 0, true, '', true);
                        $pdf->writeHTMLCell($w = 120, 0,40, '', 'Formation', 1, 0, 0, true, '', true);
                        $pdf->writeHTMLCell($w = 20,  0, 160, '', 'Arrivée', 1, 0, 0, true, 'C', true);
                        $pdf->writeHTMLCell($w = 20, 0, 180, '', 'Départ', 1, 1, 0, true, 'C', true);
                    } 

                    if(is_null($presence->getJoinTime())) {
                        $dateD = 'ABS';
                        $dateF = 'ABS';
                    } else {
                        $dateD = $presence->getJoinTime()->format('H:i');
                        if(is_null($presence->getLeftTime())) {
                            $dateF = "####-##-##";
                        } else {
                            $dateF = $presence->getLeftTime()->format('H:i');
                            $totalHeures->add(date_diff($presence->getJoinTime(), $presence->getLeftTime()));
                        }
                    }
                    // Rajouter la date sur chaque ligne
    //                $w, $h, $txt, $border=0, $align='J', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false
                    $coursName = $presence->getMeeting()->getName();
                    
                    if(strlen($coursName) > 50) {
                        $coursName = substr($coursName, 0,50).' ...';
                    }
                    
                    $pdf->writeHTMLCell($w = 30, 0, '', '', $presence->getMeeting()->getStart()->format('d/m/Y'), 1, 0, 0, true, '', true);
                    $pdf->writeHTMLCell($w = 120, 0,40, '', $coursName, 1, 0, 0, true, '', true);
                    $pdf->writeHTMLCell($w = 20,  0, 160, '', $dateD, 1, 0, 0, true, 'C', true);
                    $pdf->writeHTMLCell($w = 20, 0, 180, '', $dateF, 1, 1, 0, true, 'C', true);
                }
               
            $pdf->writeHTMLCell($w = 150,  0, '', '', 'Total heures ', 1, 0, 0, true, 'R', true);
            $pdf->writeHTMLCell($w = 40, 0, 160, '', $totalHeuresOri->diff($totalHeures)->h + ($totalHeuresOri->diff($totalHeures)->days * 24).'H '. $totalHeuresOri->diff($totalHeures)->i, 1, 1, 0, true, 'C', true); 
                
//        }
           
//        }
        
        $pdf->Output( $upload.'/'.$filename,'F'); // This will output the PDF as a response direc
        return $filename;
    }
    
    #[Route('/api/presences/date/{date}', name:'api_date_presences', methods:['GET'])]
    public function getPresencesDateApi($date, SerializerInterface $serializer): JsonResponse {
        $em = $this->managerRegistry->getManager();
        $dateStart = date_create($date)->setTime(00, 00, 00);
        $dateEnd = date_create($date)->setTime(23, 59, 59);
        
        $result = [];
        
        $meetings = $em->getRepository(Meeting::class)->filterByDate($dateStart,$dateEnd);
        
        foreach ($meetings as $meeting) {
            $id = $meeting->getId();
            $result[$id]['externalid'] = $meeting->getExternalid();
            $result[$id]['name'] = $meeting->getName();
            $result[$id]['start'] = $meeting->getStart();
            $result[$id]['end'] = $meeting->getStart();
            $result[$id]['presences'] = $em->getRepository(Joiner::class)->getPresencesByMeetingApi($id);
            
        }
        
        return new JsonResponse([
            'meetings' => $result,
            Response::HTTP_OK, 
            ['accept' => 'json'], 
            true
        ]);
    }
    
    #[Route('/api/presences/meeting/{meetingid}', name:'api_date_presences', methods:['GET'])]
    public function getPresencesMeetingApi($meetingid, SerializerInterface $serializer): JsonResponse {
        $em = $this->managerRegistry->getManager();
        
        $result = [];
        
        $meeting = $em->getRepository(Meeting::class)->findOneBy([
         'externalid'=>$meetingid 
//ou utiliser le filename en fonction de ce qu'ils ont dans moodle                
        ]) ;
        
        foreach ($meetings as $meeting) {
            $id = $meeting->getId();
            $result[$id]['externalid'] = $meeting->getExternalid();
            $result[$id]['name'] = $meeting->getName();
            $result[$id]['meetingid'] = $meeting->getFilename();
            $result[$id]['start'] = $meeting->getStart();
            $result[$id]['end'] = $meeting->getStart();
            $result[$id]['presences'] = $em->getRepository(Joiner::class)->getPresencesByMeetingApi($id);
            
        }
        
        return new JsonResponse([
            'meetings' => $result,
            Response::HTTP_OK, 
            ['accept' => 'json'], 
            true
        ]);
    }
}
