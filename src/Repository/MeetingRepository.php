<?php

namespace App\Repository;

use App\Entity\Meeting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Meeting|null find($id, $lockMode = null, $lockVersion = null)
 * @method Meeting|null findOneBy(array $criteria, array $orderBy = null)
 * @method Meeting[]    findAll()
 * @method Meeting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeetingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meeting::class);
    }

   public function filterByDate(\DateTime $beginDate, \DateTime $endDate, $query=FALSE,$options=[]) {
//       dd($options);
       
       $qb = $this->createQueryBuilder('m')
               ->join('m.joiners', 'j')
               ->where('m.start >= :beginDate')
               ->andWhere('m.start <= :endDate')
               ->andWhere('j.role LIKE \'MODERATOR\'')
               ->setParameter('beginDate', $beginDate)
               ->setParameter('endDate', $endDate);
               if(!empty($options)) {
                   $qb->andWhere($qb->expr()->like(key($options), $qb->expr()->literal(str_replace('*', '%', current($options))) ));
               }
       
               $qb->orderBy('m.start', 'DESC');
       
//          dd($qb->getQuery());
       
        if($query === FALSE) {
            return $qb->getQuery()->getResult();
        } else {
            return $qb->getQuery();
        }
   }
}
