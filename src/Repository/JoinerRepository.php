<?php

namespace App\Repository;

use App\Entity\Joiner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Joiner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Joiner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Joiner[]    findAll()
 * @method Joiner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoinerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Joiner::class);
    }

    public function getListJoiners(){
        return $this->createQueryBuilder('j')
                ->select('COUNT(j) AS nb, j.externalid, j.name')
                ->where('j.role = \'VIEWER\'' )
                ->groupBy('j.externalid')
                ->addGroupBy('j.name')
                ->getQuery()
                ->getResult()
                ;
    }
    
    public function getPresencesByJoiner($joiners=null, $start=null, $end=null){
        
        $qb = $this->createQueryBuilder('j');
        $qb->select('j,m')
            ->join('j.meeting','m');
        
            if(!is_null($joiners)) {
                $qb->where('j.externalid IN (:joiners)')
                ->setParameter('joiners', $joiners);
            }
            
            if(is_null($start)) {
                $qb->andWhere('j.jointime > :start ')
                ->setParameter('start', date('Y-m-d H:i:s'));
            } else {
                $qb->andWhere('j.jointime >= :start ')
                ->setParameter('start', $start);
            }
            
            if(!is_null($end)) {
                $qb->andWhere('j.lefttime <= :end ')
                ->setParameter('end', $end);
            } 
            $qb->andWhere('j.jointime IS NOT NULL')
               ->addOrderBy('j.name', 'ASC')
               ->addOrderBy('j.jointime', 'ASC');
            
        return $qb->getQuery()->getResult();
    }
    
     public function getPresencesByMeetingApi($meetingId){
        
        $qb = $this->createQueryBuilder('j');
        $qb->select('j.externalid, j.role, j.jointime, j.lefttime, j.name')

                ->Where('j.meeting = :meetingId')
                ->setParameter('meetingId', $meetingId)
                ->addOrderBy('j.name', 'ASC')
                ->addOrderBy('j.jointime', 'ASC');
            
        return $qb->getQuery()->getResult();
    }
    
}
