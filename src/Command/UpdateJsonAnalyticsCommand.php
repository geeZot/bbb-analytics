<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\Meeting;
use App\Entity\Joiner;

class UpdateJsonAnalyticsCommand extends Command {

    private $dashboardDir;
    private $container;
    private $role = ['VIEWER', 'MODERATOR'];


    public function __construct(ContainerInterface $container, string $dashboardDir) {
        $this->container = $container;
        $this->dashboardDir = $dashboardDir;
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setName('bbbanalytics:update:jsonmeeting')
            ->setDescription('Update bbb meetings values from json');
            $this->addOption('anonymous','a', InputOption::VALUE_OPTIONAL, 'Utilisateur anonyme', false);
        

    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $anonymous =  $input->getOption('anonymous');
        
        $em = $this->container->get('doctrine')->getManager();

        $dashboardFiles = glob('{'.$this->dashboardDir.'*'.DIRECTORY_SEPARATOR.'*'.DIRECTORY_SEPARATOR.'learning_dashboard_data.json}', GLOB_BRACE);

        foreach($dashboardFiles as $dashboardFile) {
            
            $dashboard = json_decode(file_get_contents($dashboardFile), true);

            $meetingExist = $em->getRepository('App\Entity\Meeting')->findOneBy(array('filename'=>$dashboard['intId']));

                if(is_null($meetingExist)) {

                    echo "=== START ".$dashboard['intId']." ===\n";
                    $meeting = new Meeting();
                    $meeting->setBreakout(false);
                    $meeting->setExternalid($dashboard['extId']);
                    $meeting->setFilename($dashboard['intId']);
                    $meeting->setName($dashboard['name']);
                    $meeting->setStart((new \DateTime())->setTimestamp((int)($dashboard['createdOn']/1000)) );
                    $meeting->setEnd((new \DateTime())->setTimestamp((int)($dashboard['endedOn']/1000)));


                    $meeting->setReason('ENDED_AFTER_USER_LOGGED_OUT');
                    $duree = date_diff($meeting->getEnd(), $meeting->getStart());

                    $meeting->setDuree($duree);
                    $em->persist($meeting);
    //
                    // Traitement des infos utilisateurs
                    $joinersInfos=[];
                    foreach ($dashboard['users'] as $user) {
                        
                        $joinersInfos[$user['extId']]['isModerator'] = (int)$user['isModerator'];
                        $joinersInfos[$user['extId']]['name'] = is_null($anonymous)?uniqid():$user['name'];
                        foreach ($user['intIds'] as $intId) {
                            if (!isset($joinersInfos[$user['extId']]['registeredOn']) || $joinersInfos[$user['extId']]['registeredOn'] > $intId['registeredOn']) {
                                $joinersInfos[$user['extId']]['registeredOn'] = $intId['registeredOn'];
                            }

                            if (!isset($joinersInfos[$user['extId']]['leftOn']) || $joinersInfos[$user['extId']]['leftOn'] < (int)$intId['leftOn']) {
                                $joinersInfos[$user['extId']]['leftOn'] = $intId['leftOn'];
                            }

                        }

                    }
                    
                    foreach ($joinersInfos as $extId => $joinerInfos) {
                        $joiner = $this->addJoiner($extId, $joinerInfos, $meeting);
                        $em->persist($joiner);
                    }

                $em->flush();

                echo "=== END ".$dashboard['intId']." ===\n";
            }
        }
        return Command::SUCCESS;
    }

    private function addJoiner($extId, $joinerInfos, $meeting) {
        $joiner = new Joiner();
        $joiner->setExternalid($extId);
        $joiner->setRole($this->role[$joinerInfos['isModerator']]);
        $joiner->setName($joinerInfos['name']);
        $joiner->setJointime((new \DateTime())->setTimestamp((int)($joinerInfos['registeredOn']/1000)));
        $joiner->setLefttime((new \DateTime())->setTimestamp((int)($joinerInfos['leftOn']/1000)));

        $joiner->setMeeting($meeting);
        echo $joiner->getName().' '.$joiner->getRole().' '.$joiner->getJointime()->format('d/m/Y H:i:s').' '.$joiner->getLefttime()->format('d/m/Y H:i:s').chr(10);
        return $joiner;
    }
    
}
