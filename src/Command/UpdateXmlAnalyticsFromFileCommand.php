<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\Meeting;
use App\Entity\Joiner;

class UpdateXmlAnalyticsFromFileCommand extends Command {

    private $eventsDir;
    private $container;

    public function __construct(ContainerInterface $container, string $eventsDir) {
        $this->container = $container;
        $this->eventsDir = $eventsDir;
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setName('bbbanalytics:update:ffxmlmeeting')
            ->setDescription('Update bbb meetings values from xml file');
            $this->addOption('anonymous','a', InputOption::VALUE_OPTIONAL, 'Utilisateur anonyme', false);
        

    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $anonymous =  $input->getOption('anonymous');
        
        $em = $this->container->get('doctrine')->getManager();

        $meetingFiles= array_diff(scandir($this->eventsDir), array('..', '.'));
        
        foreach($meetingFiles as $meetingFile) {
            
            $recording = simplexml_load_file($this->eventsDir.$meetingFile);
            $meetingExist = $em->getRepository('App\Entity\Meeting')->findOneBy(array('externalid'=>$recording->meeting['externalId']));
            
                if(is_null($meetingExist)) {

                $usersById = array();
                $userTime = array();
                $userTimeByName = array();

                
                echo "=== START ".$recording['meeting_id']." ===\n";

                $participantsModule = $recording->xpath('/recording/event[@module="PARTICIPANT"]');
                $voiceModule = $recording->xpath('/recording/event[@module="VOICE"]');
                $chatModule = $recording->xpath('/recording/event[@module="CHAT"]');
                $startEvent = $recording->xpath('/recording/event[@eventname="CreatePresentationPodEvent"]');
                $endEvent = $recording->xpath('/recording/event[@eventname="EndAndKickAllEvent"]');                                                 

                $events = array_merge($participantsModule,$voiceModule,$chatModule);
                
                if((string)$endEvent[0]->reason === 'ENDED_AFTER_EXCEEDING_DURATION') {
                    $lastEvent = $recording->xpath('/recording/event[last()-1]');
                    $eventEnded = $lastEvent[0]->date;
                } else {
                    $eventEnded = $endEvent[0]->date;
                }

                $meeting = new Meeting();
                $meeting->setBreakout(filter_var((string)$recording->meeting['breakout'],FILTER_VALIDATE_BOOLEAN));
                $meeting->setExternalid((string)$recording->meeting['externalId']);
                $meeting->setFilename($meetingFile);
                $meeting->setName((string)$recording->meeting['name']);
                $meeting->setStart(date_create($startEvent[0]->date));
                $meeting->setEnd(date_create($eventEnded));
                $meeting->setReason((string)$endEvent[0]->reason);

                $duree = date_diff($meeting->getEnd(), $meeting->getStart());

                $meeting->setDuree($duree);

                $em->persist($meeting);

                // Traitement des infos utilisateurs

                $participantsIds=[];
                $externalUsersEvents=[];
                foreach($events as $event ) {
                   
                    if(isset($event->userId)||isset($event->participant)) {
                            if($event->attributes()['eventname'] == 'ParticipantJoinEvent') {
                                $participantsIds[(string)$event->userId]=(string)$event->externalUserId;
                                if(!isset($externalUsersEvents[$participantsIds[(string)$event->userId]][(string)$event->attributes()['eventname']])) {
                                    $externalUsersEvents[$participantsIds[(string)$event->userId]][(string)$event->attributes()['eventname']]=(string)$event->date;
                                    $externalUsersEvents[$participantsIds[(string)$event->userId]]['name']=(string)$event->name;
                                    $externalUsersEvents[$participantsIds[(string)$event->userId]]['role']=(string)$event->role;
                                    $externalUsersEvents[$participantsIds[(string)$event->userId]]['last_event']=(string)$event->date;
                                }
                            } elseif(isset($participantsIds[(string) $event->userId])) {
                                $externalUsersEvents[$participantsIds[(string)$event->userId]][(string)$event->attributes()['eventname']]=(string)$event->date;
                                $externalUsersEvents[$participantsIds[(string)$event->userId]]['last_event']=(string)$event->date;
                            } elseif(isset($event->senderId)) {
                                $externalUsersEvents[$participantsIds[(string)$event->senderId]][(string)$event->attributes()['eventname']]=(string)$event->date;
                                $externalUsersEvents[$participantsIds[(string)$event->senderId]]['last_event']=(string)$event->date;
                            } elseif(isset($event->participant)) {
                                $externalUsersEvents[$participantsIds[(string)$event->participant]][(string)$event->attributes()['eventname']]=(string)$event->date;
                                $externalUsersEvents[$participantsIds[(string)$event->participant]]['last_event']=(string)$event->date;
                            } 

                    }      

                }
                unset($externalUsersEvents['']);
                
                foreach ($externalUsersEvents as $externalUserId => $usersEvents) {

                    echo $usersEvents['name']."\t"
                        .$usersEvents['role']."\t"
                        .((isset($usersEvents['ParticipantJoinedEvent']))?$usersEvents['ParticipantJoinedEvent']:'')."\t"
                        .$usersEvents['ParticipantJoinEvent']."\t"
                        .((isset($usersEvents['ParticipantLeftEvent']))?$usersEvents['ParticipantLeftEvent']:'')."\t"
                        .$externalUserId."\t"
                        .$usersEvents['last_event']."\n";

                    $joiner = new Joiner();
                    $joiner->setExternalid($externalUserId);
                    $joiner->setRole((string) $usersEvents['role']);
                    $joiner->setName(is_null($anonymous)?uniqid():(string) $usersEvents['name']);
                    $joiner->setJointime(new \DateTime($usersEvents['ParticipantJoinEvent']));
                    $joiner->setLefttime(new \DateTime($usersEvents['last_event']));
                    $joiner->setMeeting($meeting);

                    $em->persist($joiner);
                    
                }
                
                $em->flush();

                echo "=== END ".$recording['meeting_id']." ===\n";
            }
        }
        return Command::SUCCESS;
    }

}
