<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200629141557 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joiner ADD meeting_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE joiner ADD CONSTRAINT FK_A129BC5267433D9C FOREIGN KEY (meeting_id) REFERENCES meeting (id)');
        $this->addSql('CREATE INDEX IDX_A129BC5267433D9C ON joiner (meeting_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joiner DROP FOREIGN KEY FK_A129BC5267433D9C');
        $this->addSql('DROP INDEX IDX_A129BC5267433D9C ON joiner');
        $this->addSql('ALTER TABLE joiner DROP meeting_id');
    }
}
